import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Employee } from './models/employee';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  
 
  API_KEY = 'MY_API_KEY';

  constructor(private HTTpClient: HttpClient) { }

  ROOT_URI = 'api/employee.get.js';

  public getEmployees(): Observable<Employee[]>{
    return this.HTTpClient.get<Employee[]>("http://localhost:3000/api/employee")
  }

}
 