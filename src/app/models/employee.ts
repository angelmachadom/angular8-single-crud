export class Employee {
    id: number;
    name: string;
    country: string;
    email: string;
    phone: number;
}
