import { Component, OnInit } from '@angular/core';
import { EmployeeService } from "./employee.service";
import { Observable } from 'rxjs';
 
import { Employee } from "./models/employee"
import { rendererTypeName } from '@angular/compiler';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  employee:Object;

  constructor(private EmployeeService : EmployeeService) {}

  ngOnInit(){

    this.EmployeeService.getEmployees()
      .subscribe(data => this.employeeArray= data);
    
  }


  employeeArray: Employee [] = [
    ,
    {id: 1, name: "Ryan", country: "USA", email: "ryanair@ryanair.com", phone: 651692467},
    {id: 1, name: "Ryan", country: "USA", email: "ryanair@ryanair.com", phone: 651692467},
    {id: 2, name: "Stalone", country: "UK", email: "stalone@ryanair.com", phone: 653682464},
    {id: 3, name: "Kobe", country: "USA", email: "kobebryant@ryanair.com", phone: 699373338}
  ];

  selectedEmployee: Employee = new Employee();
  
  openForEdit(employee : Employee){
    this.selectedEmployee = employee;
  }

  addOrEdit(){
    if(this.selectedEmployee.id === 0){
      this.selectedEmployee.id = this.employeeArray.length + 1;
      this.employeeArray.push(this.selectedEmployee);
    }
    this.selectedEmployee = new Employee();
  }
  
  delete(){
    if(confirm('Are you want to delete it?')){
    this.employeeArray = this.employeeArray.filter(x => x != this.selectedEmployee);
    this.selectedEmployee = new Employee;
    }
  }

}


